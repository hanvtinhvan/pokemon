import Reflux from 'reflux';

const Actions = Reflux.createActions([
    'getPokemons',
    'getPokemon',
    'getmyPokemon',
    'savePokemon',
    'releasePokemon'
]);

export default Actions;
