export const BASE_URL = 'https://pokeapi.co/api/v2/pokemon/'
export function get(url) {
    return fetch(url)
        .then((response) => {
            return response.json();
        }).catch(e=> {
            return e
        });
}