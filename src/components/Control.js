import React from 'react';
import Reflux from 'reflux';

import PokemonStore from '../stores/PokemonStore';
import Actions from '../actions/Actions';

class Control extends Reflux.Component {
  constructor() {
    super();

    this.state = {};
    this.store = PokemonStore;
    this.storeKeys = ['control'];
  }

  handlePagiationPage (e, type) {
    if(!e) return;
    const {previous, next} = this.state.control;
    if (type === 'NEXT') {
      Actions.getPokemons(next)
    } else {
      Actions.getPokemons(previous)
    }
  }
  render() {
    return (
        <div className="row text-center" style={{marginBottom: '3rem'}}>
            <button className="btn btn-primary"
              onClick={(e) => this.handlePagiationPage(e, 'PREV')}
              style={{marginRight: '1rem'}}
            >prev</button>
            <button className="btn btn-primary"
              onClick={(e) => this.handlePagiationPage(e, 'NEXT')}
            >next</button>
        </div>
    );
  }
}

export default Control;
