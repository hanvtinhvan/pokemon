import React, { Component } from 'react';
import PropTypes from 'prop-types';

import PokemonType from './PokemonType';

class PokemonTabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pokemon: {
        types: [],
        abilities: [],
        moves: []
      }
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({pokemon: nextProps.pokemon});
  }

  getTypes() {
    const {types} = this.state.pokemon;
    let _types = types.map((currentValue, index, array) => {
      return (
        <PokemonType
          key={ index }
          name={ currentValue.type.name }
          />
      );
    });

    return _types;
  }

  getAbilities() {
    const {abilities} = this.state.pokemon;
    let _abilities = abilities.map((currentValue, index, array) => {
      return (
        <li key={ index }> { currentValue.ability.name } </li>
      );
    });

    return _abilities;
  }

  getMoves() {
    const {moves} = this.state.pokemon;
    let _moves = moves.map((currentValue, index, array) => {
      return (
        <div className="col-sm-3" key={index}>- { currentValue.move.name }</div>
      );
    });

    return _moves;
  }

  render() {
    return (
      <div className="col-md-6">
        <ul id="MyTabs" className="nav nav-tabs nav-justified" role="tablist">
          <li role="presentation"  className="active"><a href="#data" aria-controls="data" role="tab" data-toggle="tab">Data</a></li>
          {/* <li role="presentation"><a href="#stats" aria-controls="stats" role="tab" data-toggle="tab">Stats</a></li> */}
          <li role="presentation"><a href="#moves" aria-controls="moves" role="tab" data-toggle="tab">Moves</a></li>
        </ul>

        <div className="tab-content">
          <div role="tabpanel" className="tab-pane active" id="data">
            <ul className="list-group">

              <li className="list-group-item">
                <strong>Type</strong>
                <span className="pull-right">
                  { this.getTypes() }
                </span>
              </li>

              <li className="list-group-item">
                <strong>Height</strong>
                <span className="pull-right">{ this.props.pokemon.height }</span>
              </li>

              <li className="list-group-item">
                <strong>Weight</strong>
                <span className="pull-right">{ this.state.pokemon.weight }</span>
              </li>

              <li className="list-group-item">
                <strong>Abilities</strong>
                <ul>
                  { this.getAbilities() }
                </ul>
              </li>
            </ul>
          </div>

          <div role="tabpanel" className="tab-pane" id="moves">
            { this.getMoves() }
          </div>
        </div>
      </div>
    );
  }
}

PokemonTabs.propTypes = {
  pokemon: PropTypes.any.isRequired
}

export default PokemonTabs;
