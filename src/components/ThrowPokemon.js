import React from 'react';
import Reflux from 'reflux';

import PokemonStore from '../stores/PokemonStore';
import Actions from '../actions/Actions';
import {getRandomInt, getFullDateTime} from '../utils/utils'
class ThrowPokemon extends Reflux.Component {
    constructor(props) {
        super(props);

        this.state = {
            isCaught: false,
            newname: ''
        };
        this.store = PokemonStore;
        this.storeKeys = ['myPokemon'];
    }



    throwPokeBall = (e) => {
        if (!e) return
        if (getRandomInt(2) === 1) {
            this.setState({
                isCaught: true
            });
            alert('Gotcha! You caught the Pokemon!')
        }
        else
            alert('Oooooppssss. Pokemon broke the pokeball!')
    };

    savePokemon = (e) => {
        if (!e) return;
        let newdata = this.props.pokemon
        newdata.name = this.state.newname
        newdata.idcur = getFullDateTime()

        const actionSavePokemon = async () => await Actions.savePokemon(newdata)
        actionSavePokemon()
        alert('Congratulations')
        window.location.href = '/'
    }

    render() {
        var { isCaught, newname } = this.state
        return (
            <div className="col-md-12 text-center" style={{marginTop: '20px'}}>
                {!isCaught ?(
                    <button type="button" className="btn btn-success" onClick={(e) => this.throwPokeBall(e)}>Throw PokeBall</button>
                ) : (
                    <div>
                        <input type="text" style={{padding: '7px 12px'}} placeholder="Input name" value={newname} onChange={(event) => { this.setState({ newname: event.target.value }) }}></input>
                        <button type="button" className="btn btn-info" onClick={(e) => this.savePokemon(e)}>Catch</button>
                    </div>
                )}
            </div>
        );
    }
}

export default ThrowPokemon;
