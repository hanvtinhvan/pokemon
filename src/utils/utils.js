function getIdFromUrl(currentValue) {
	if (!currentValue) return ''
	const {url} = currentValue;
	return url.split('/')[6]
}

function getUrlImg (id) {
	if (!id) return ''
	return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`
}

function getRandomInt (max){
	if (!max) return 0
	return Math.floor(Math.random() * Math.floor(max));
};

function getFullDateTime () {
	var currentdate = new Date()
	var datetime = currentdate.getDate().toString()
	+ (currentdate.getMonth() + 1).toString()
	+ currentdate.getFullYear()
	+ currentdate.getHours()
	+ currentdate.getMinutes()
	+ currentdate.getSeconds();
	return datetime
}

export {
	getIdFromUrl,
	getUrlImg,
	getRandomInt,
	getFullDateTime
}