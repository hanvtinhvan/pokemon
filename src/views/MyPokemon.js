import React from 'react';
import Reflux from 'reflux';

import PokemonStore from '../stores/PokemonStore';
import Actions from '../actions/Actions';

import Header from '../components/Header';
import { getUrlImg } from '../utils/utils';

class MyPokemon extends Reflux.Component {
    constructor(props) {
        super(props);

        this.state = {

        };
        this.store = PokemonStore;
        this.storeKeys = ['myPokemon'];
    }

    componentDidMount() {
        this.fetchMyPokemon()
    }

    fetchMyPokemon () {
        Actions.getmyPokemon()
    }

    componentDidUpdate(prevProps, prevState) {

    }

    releasePokemon(idcur) {
        const releasePokemon = async () => await Actions.releasePokemon(idcur)
        releasePokemon()
        this.fetchMyPokemon()
    }

    handleRelease(e, idcur) {
        if (!e) {
            return
        }
        this.releasePokemon(idcur)
    }

    getMyPokemon() {
        var { myPokemon } = this.state
        if (myPokemon !== null && myPokemon.length > 0){
            return  this.state.myPokemon.map((currentValue, index, array) => {
                const {id, name, idcur} = currentValue;
                return (
                    <tr key={index}>
                        <td><img alt={name} className="img-responsive" src={getUrlImg(id)} /></td>
                        <td>{name}</td>
                        <td><button type="button" className="btn btn-danger" onClick={(e) => this.handleRelease(e, idcur)}>Release</button></td>
                    </tr>
                )
            })
        }
        else{
            return <tr><td colSpan="3" style={{textAlign: 'center'}}>You don't have pokemon</td></tr>
        }
    }

    render() {
        return (
            <div className="MyPokemon">
                <Header title="MyPokemon" />

                <table id="table-pokemon">
                    <thead>
                        <tr>
                            <th style={{width: '20%'}}></th>
                            <th style={{width: '30%'}}>Name</th>
                            <th style={{width: '50%'}}>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        { this.getMyPokemon() }
                    </tbody>
                </table>

            </div>
        );
    }
}

export default MyPokemon;
