import React from 'react';
import Reflux from 'reflux';

import PokemonStore from '../stores/PokemonStore';
import Actions from '../actions/Actions';

import Header from '../components/Header';
import PokemonCard from '../components/PokemonCard';
import Control from '../components/Control';
import {getIdFromUrl, getUrlImg} from '../utils/utils'

class Home extends Reflux.Component {
    constructor() {
        super();

        this.state = {};
        this.store = PokemonStore;
        this.storeKeys = ['data'];
    }

    fetchPokemonFromAPI() {
        Actions.getPokemons('http://pokeapi.co/api/v2/pokemon/')
    }

    componentDidMount() {
        this.fetchPokemonFromAPI()
    }

    renderNullView() {
        return <div className="text-center">Not found data</div>
    }

    fetchPokemon() {
        let myPokemon =  sessionStorage.getItem('myPokemon') ? JSON.parse(sessionStorage.getItem('myPokemon')) : []
        const {data} = this.state
        if (data.length > 0) {
            let pokemons = data.map((currentValue, index, array) => {
                const idPokemon = getIdFromUrl(currentValue) ? getIdFromUrl(currentValue) : ''
                var count = 0
                if(myPokemon !== null && myPokemon.count !== 0)
                    myPokemon.filter((myPokemon) => {
                        if (myPokemon.id === + idPokemon)
                            count++
                        return myPokemon.id === + idPokemon
                    })
                return(
                    <PokemonCard
                        key={ index }
                        avatar={getUrlImg(getIdFromUrl(currentValue)) }
                        pokemon={ currentValue }
                        count={count}
                        />
                );
            });
            return pokemons
        }
            
        else
             return this.renderNullView()

    }

    render() {
        const { data } = this.state
        return (
            <div className="Home">
                <Header title="Pokedex" />

                {
                    data.length > 0 && <Control />
                }

                <div className="row">
                    { this.fetchPokemon() }
                </div>
            </div>
        );
    }
}

export default Home;
