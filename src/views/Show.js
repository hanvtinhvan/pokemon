import React from 'react';
import Reflux from 'reflux';

import PokemonStore from '../stores/PokemonStore';
import Actions from '../actions/Actions';

import Header from '../components/Header';
import PokemonImage from '../components/PokemonImage';
import PokemonTabs from '../components/PokemonTabs';
import ThrowPokemon from '../components/ThrowPokemon';
import {getUrlImg} from '../utils/utils'

class Show extends Reflux.Component {
    constructor(props) {
        super(props);

        this.state = {

        };
        this.store = PokemonStore;
        this.storeKeys = ['dataPokemon'];
    }

    componentDidMount() {
        Actions.getPokemon(this.props.match.params.name);
    }

    componentDidUpdate(prevProps, prevState) {
    }

    render() {
        const {id} = this.state.dataPokemon
        return (
            <div className="Show">
                <Header
                    title={ this.props.match.params.name }
                    isPokemon={ true }
                    id={ id }
                />

                <div className="row">
                    <PokemonImage
                        avatar={ getUrlImg(id) }
                        />

                    <PokemonTabs
                        pokemon={ this.state.dataPokemon }
                        />
                </div>
                <div className="row">
                    <ThrowPokemon pokemon={ this.state.dataPokemon } />
                </div>

            </div>
        );
    }
}

export default Show;
