import Reflux from 'reflux';
import Actions from '../actions/Actions';
import * as HTTP from '../services/http';
class PokemonStore extends Reflux.Store {
    constructor() {
        super();

        this.listenables = Actions;

        this.state = {
            data: [],
            dataPokemon: {
            },
            dataDescription: {},
            control: {
                previous: '',
                next: ''
            },
            myPokemon: []
        }
    }

    getPokemons(url) {
        const self = this;
        HTTP.get(url).then((response) => {
            console.log(response);
            self.setState({
                data: response.results,
                control: {
                    previous: response.previous,
                    next: response.next
                }
            })
            console.log('==data',this.state.data);
        }).catch(e => {
            alert (e)
        });
    }

    getMyPokemonSession () {
        return sessionStorage.getItem('myPokemon')
            ? JSON.parse(sessionStorage.getItem('myPokemon')) : []
    }

    getPokemon(name) {
        const self = this;

        HTTP.get(HTTP.BASE_URL + name).then((response) => {
            self.setState({ dataPokemon: response });
        });
    }

    savePokemon(obj) {
        var cur = this.getMyPokemonSession()
        cur.push(obj)
        sessionStorage.setItem('myPokemon', JSON.stringify(cur))
    }

    getmyPokemon() {
        this.setState({ myPokemon: this.getMyPokemonSession() })
    }

    releasePokemon(idcur) {
        var currentmypokemon = this.getMyPokemonSession()
        var filter = currentmypokemon.filter((myPokemon) => {
            return myPokemon.idcur !== idcur;
        })
        sessionStorage.setItem('myPokemon', JSON.stringify(filter))
    }
}

export default PokemonStore;
